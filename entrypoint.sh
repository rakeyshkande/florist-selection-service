#!/bin/sh
#
# docker-entrypoint for service

set -e

echo "Executing java ${JAVA_ARGS} "$@""
java ${JAVA_ARGS} -jar -Xms512m -Xmx4096m florist-selection-service.jar

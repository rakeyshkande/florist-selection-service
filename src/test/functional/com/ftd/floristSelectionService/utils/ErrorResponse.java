package com.ftd.floristSelectionService.utils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
    private String timeStamp;

    private String exception;

    private String errorType;

    private Errors[] errors;

    private String status;

}

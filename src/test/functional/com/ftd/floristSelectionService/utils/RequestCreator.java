package com.ftd.floristSelectionService.utils;

import java.math.BigDecimal;
import java.util.HashMap;

import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiRequest.Product;
import com.ftd.floristselection.api.FloristSelectionApiRequest.Recipient;
import com.ftd.floristselection.api.FloristSelectionApiRequest.RecipientAddress;

public class RequestCreator {

    public static FloristSelectionApiRequest createfloristSelectionRequest(HashMap<String, Object> excelData) {

        FloristSelectionApiRequest finalRequest = new FloristSelectionApiRequest();
        int startDate = Integer.parseInt(excelData.get("deliveryDate").toString());
        int endDate = Integer.parseInt(excelData.get("deliveryDateEnd").toString());
        finalRequest.setDeliveryDate(CommonMethods.dateConversion(CommonMethods.getDate(startDate).toString()));
        finalRequest.setDeliveryDateEnd(CommonMethods.dateConversion(CommonMethods.getDate(endDate).toString()));
        if (!(excelData.get("excludedFlorists").toString().equals("NULL"))) {
            finalRequest
                    .setExcludedFlorists(CommonMethods.stringToList(excelData.get("excludedFlorists").toString(), ","));
        }
        finalRequest.setProduct(setProductInfo(excelData));
        finalRequest.setRecipient(createRecipentAddress(excelData));
        finalRequest.setSendingMemberCode(excelData.get("sendingMemberCode").toString());
        finalRequest.setSourceCode(excelData.get("sourceCode").toString());

        return finalRequest;
    }

    public static Recipient createRecipentAddress(HashMap<String, Object> excelData) {
        Recipient recipInfo = new Recipient();
        recipInfo.setAddress(creatAddress(excelData));
        return recipInfo;

    }

    public static RecipientAddress creatAddress(HashMap<String, Object> excelData) {
        RecipientAddress address = new RecipientAddress();
        address.setPostalCode(excelData.get("recipientZip").toString());
        return address;

    }

    public static Product setProductInfo(HashMap<String, Object> excelData) {
        Product product = new Product();
        product.setCode(excelData.get("productCode").toString());
        product.setPrice(new BigDecimal(excelData.get("productPrice").toString()));
        return product;
    }

}

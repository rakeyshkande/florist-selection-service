package com.ftd.floristSelectionService.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CommonMethods {

    public static String getDate(int numberofdays) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, numberofdays);
        String date = dateFormat.format(c.getTime());
        date = date.replace("/", "-");
        return date;
    }

    public static List<String> stringToList(String s, String delim) {
        String[] temp = s.split(delim);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < temp.length; i++) {
            list.add(temp[i].trim());
        }
        return list;
    }

    public static LocalDate dateConversion(String s) {
        // final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
        final LocalDate dt = LocalDate.parse(s);
        return dt;
    }

}

package com.ftd.floristSelectionService.automationScripts;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.ftd.automation.framework.base.RestAssuredBaseTest;
import com.ftd.automation.framework.config.ReadPropertyFile;
import com.ftd.automation.framework.report.Report;
import com.ftd.automation.framework.utilities.ExcelData;
import com.ftd.floristSelectionService.utils.CommonMethods;
import com.ftd.floristSelectionService.utils.ErrorResponse;
import com.ftd.floristSelectionService.utils.RequestCreator;
import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiResponse;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GetAvailableFlorist extends RestAssuredBaseTest {
    private String resourcePath = "src/test/resources";
    private int sheetNumber = 0;
    private int headcount = 1;
    private static final int SUCCESS_CODE = 200;
    private List<String> expectedFlorist;

    @Test(dataProvider = "testdata", testName = "Create Access Levels", description = "This test is to create the AccessLevels")
    public void getAvailableFlorist(HashMap<String, Object> testdata) {
        Report.createTest((String) testdata.get("TestCaseID"), (String) testdata.get("TestDescription"));
        expectedFlorist = CommonMethods.stringToList(testdata.get("expectedFlorist").toString(), ",");
        FloristSelectionApiRequest floristSelectionApiRequest = RequestCreator.createfloristSelectionRequest(testdata);
        Response response = given().contentType(ContentType.JSON).log().all().when().body(floristSelectionApiRequest)
                .post();
        response.then().log().all();
        int statusCode = response.getStatusCode();
        if (statusCode == SUCCESS_CODE) {
            FloristSelectionApiResponse floristSelectionApiResponse = response.then().extract()
                    .as(FloristSelectionApiResponse.class);
            if (expectedFlorist.contains(floristSelectionApiResponse.getSelectedMember())) {
                Report.info("The Florist from the response " + floristSelectionApiResponse.getSelectedMember()
                        + " is available in the expected list of florists :" + expectedFlorist);
            } else {
            	System.out.println("The test case is failed becuase, The expected florist list is"+ expectedFlorist+
            			" but the florist in the response is : "+floristSelectionApiResponse.getSelectedMember());
                Report.info("The Florist from the response " + floristSelectionApiResponse.getSelectedMember()
                        + " is not available in the expected list of florists :" + expectedFlorist);
                assertTrue(false);
            }

        } else {
            ErrorResponse errorResponse = response.then().extract().as(ErrorResponse.class);
            Report.info("The test failed because of the error" + errorResponse.getErrors()[0].getMessage());
            Report.info("The test failed because of the error" + errorResponse.getErrors()[0].getKey());
        }

    }

    @DataProvider(name = "testdata")
    public Object[][] readTestData() {
        Object[][] testdata = null;
        testdata = ExcelData.getData(
                resourcePath + "/" + ReadPropertyFile.getProp().getProperty("ENV") + "/getAvailableFloirst.xlsx",
                sheetNumber, headcount, "P0");
        return testdata;
    }

}

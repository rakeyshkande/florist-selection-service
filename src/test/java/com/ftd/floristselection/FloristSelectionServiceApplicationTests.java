package com.ftd.floristselection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test cases
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FloristSelectionServiceApplication.class)
public class FloristSelectionServiceApplicationTests {

    @Test
    public void contextLoads() {

    }
}

package com.ftd.floristselection.ws.client.floristavailability;

import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.time.LocalDate;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AvailabilityServiceImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(AvailabilityServiceImplTest.class);
    private static final double PRICE = 12.12;
    private static final int DAYS = 1;
    @Autowired
    private AvailabilityService availabilityService;

    @Test
    public void testGetAvailableFlorists() {

        try {

            Flux<AvailabilityInfoResponse> response = availabilityService.getAvailableFlorists("ftd",
                    "1212",
                    BigDecimal.valueOf(PRICE), "60515", LocalDate.now(), LocalDate.now().plusDays(DAYS));
            LOG.info("getAvailableFlorists## {}", response);

        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage());
        }
    }
}
package com.ftd.floristselection.ws.client;

import com.ftd.floristselection.ws.client.member.MemberService;
import com.ftd.floristselection.ws.client.member.domain.IdentityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.MemberInfoResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberServiceTest {
    private static final Logger LOG = LoggerFactory.getLogger(MemberServiceTest.class);
    @Autowired
    private MemberService memberService;

    @Test
    public void testGetMemberInfo() {
        try {
            Mono<MemberInfoResponse> memberInfo = memberService.getMemberInfo("ftd", "1377312590");

            LOG.info("getMemberInfo## {}", memberInfo.block());
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage());
        }

    }

    @Test
    public void testGetIdentityInfo() {
        try {
            List<String> codes = Arrays.asList("01-0120AA", "01-0059AA");
            Flux<IdentityInfoResponse> identityEntityFlux = memberService.getIdentityInfo("ftd", codes);
            LOG.info("identityInfo## {}", identityEntityFlux.collectList().block());
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage());
        }

    }
}

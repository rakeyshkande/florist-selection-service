package com.ftd.floristselection.ws.client.ordersearch;

import com.ftd.floristselection.util.FloristSelectionUtil;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderSearchServiceImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(OrderSearchServiceImplTest.class);
    @Autowired
    private OrderSearchService orderSearchService;

    @Test
    public void testGetSameDaySameRecipientFlorists() {

        try {
            Mono<OrderQueryResponse> response = orderSearchService.getSameDaySameRecipientFlorists(FloristSelectionUtil
                    .getOrderSearchRequest(LocalDate.now(), "3113 Woodcreek Dr", "60515"));
            LOG.debug("getSameDaySameRecipientFlorists## {}", response.block());
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage());
        }
    }
}
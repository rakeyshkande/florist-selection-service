package com.ftd.floristselection.ws.client.oscarselector;

//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class OscarSelectorServiceImplTest {
//
//    private static final Logger LOG = LoggerFactory.getLogger(OscarSelectorServiceImplTest.class);
//    @Autowired
//    private OscarSelectorService oscarSelectorService;
//
//    private static final BigDecimal LATITUTE = new BigDecimal(41.85948000);
//    private static final BigDecimal LONGITUTE = new BigDecimal(-87.97279000);
//
//    @Test
//    public void testSelectFillingMember() throws Exception {
//
//        ObjectFactory objectFactory = new ObjectFactory();
//
//        SelectFillingMember selectFillingMember = objectFactory.createSelectFillingMember();
//        AthenaRequest athenaRequest = objectFactory.createAthenaRequest();
//        athenaRequest.setClientCode("TEST123");
//        athenaRequest.setClientReference("O1234B");
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        Date dob = df.parse("2019-02-22");
//        XMLGregorianCalendar xmlDate =
//                DatatypeFactory.newInstance().newXMLGregorianCalendar(df.format(dob));
//        athenaRequest.setDeliveryDate(xmlDate);
//
//        AthenaRequestedFillingMember athenaRequestedFillingMember =
//                objectFactory.createAthenaRequestedFillingMember();
//
//        athenaRequestedFillingMember.setDistanceToRecipient(1.0f);
//        athenaRequestedFillingMember.setMemberCode("12-1970AA");
//
//        athenaRequest.getFillingMember().add(athenaRequestedFillingMember);
//
//        athenaRequest.setRecipientLatitude(LATITUTE);
//        athenaRequest.setRecipientLongitude(LONGITUTE);
//        athenaRequest.setScenarioGroupCode("Launch_2018_NPS");
//        athenaRequest.setSendingMemberCode("90-8400AA");
//
//        selectFillingMember.setRequest(athenaRequest);
//
//        oscarSelectorService.selectFillingMember(selectFillingMember);
//
//    }
//}
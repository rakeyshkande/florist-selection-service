package com.ftd.floristselection.api;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FloristSelectionApiRequestTest {

    private FloristSelectionApiRequest floristSelectionApiRequest = null;
    private FloristSelectionApiRequest.Product product = null;
    private FloristSelectionApiRequest.Recipient recipient = null;
    private FloristSelectionApiRequest.RecipientAddress recipientAddress = null;
    private FloristSelectionApiRequest.AddressType addressType = null;
    private FloristSelectionApiRequest.GeoCode geoCode = null;
    private static final int LAT_CORD = 1;
    private static final int LON_CORD = 2;
    private static final double PRICE = 12.12;

    @Before
    public void init() {
        floristSelectionApiRequest = new FloristSelectionApiRequest();
        geoCode = new FloristSelectionApiRequest.GeoCode();
        addressType = new FloristSelectionApiRequest.AddressType();
        recipientAddress = new FloristSelectionApiRequest.RecipientAddress();
        recipient = new FloristSelectionApiRequest.Recipient();
        product = new FloristSelectionApiRequest.Product();
    }

    @Test
    public void execute() {
        geoCode.setLatitude(BigDecimal.valueOf(LAT_CORD));
        geoCode.setLongitude(BigDecimal.valueOf(LON_CORD));

        addressType.setInfo("TestInfo");
        addressType.setName("TestName");
        addressType.setType("T type");

        recipientAddress.setAddressLine1("3113 Woodcreek Dr");
        recipientAddress.setAddressLine2("12A");
        recipientAddress.setAddressType(addressType);
        recipientAddress.setCityName("Chicago");
        recipientAddress.setPostalCode("60616");
        recipientAddress.setCountryCode("US");
        recipientAddress.setCountryCodeISO("USA");
        recipientAddress.setCountryName("America");
        recipientAddress.setStateCode("IL");
        recipientAddress.setStateName("Illinois");
        recipientAddress.setGeoCode(geoCode);
        recipientAddress.toString();

        recipient.setAddress(recipientAddress);
        recipient.setFirstName("Test F");
        recipient.setLastName("Test L");
        recipient.setMiddleName("M");
        recipient.setTimeOfService("AM");
        recipient.setTitle("Mr");
        recipient.toString();

        product.setPrice(BigDecimal.valueOf(PRICE));
        product.setCode("12AA");
        product.setAlternateProductCode("12AB");
        product.toString();

        floristSelectionApiRequest.setSourceCode("Test");
        floristSelectionApiRequest.setExcludedFlorists(new ArrayList<>());
        floristSelectionApiRequest.setRecipient(recipient);
        floristSelectionApiRequest.setProduct(product);
        floristSelectionApiRequest.setDeliveryDateEnd(LocalDate.now());
        floristSelectionApiRequest.setDeliveryDate(LocalDate.now());
        floristSelectionApiRequest.setSendingMemberCode("90-9085AA");
        floristSelectionApiRequest.setLineOrderNo("Line1");
        floristSelectionApiRequest.setMasterOrderNo("Master1");
        floristSelectionApiRequest.toString();

        assertThat(floristSelectionApiRequest.getSourceCode().equalsIgnoreCase("Test"));
    }
}

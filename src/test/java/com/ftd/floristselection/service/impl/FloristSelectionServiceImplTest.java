package com.ftd.floristselection.service.impl;

import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiResponse;
import com.ftd.floristselection.api.entity.SourceCodeInfo;
import com.ftd.floristselection.config.AppConfig;
import com.ftd.floristselection.repository.SourceCodeRepository;
import com.ftd.floristselection.ws.client.floristavailability.AvailabilityService;
import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import com.ftd.floristselection.ws.client.member.MemberService;
import com.ftd.floristselection.ws.client.ordersearch.OrderSearchService;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import com.ftd.floristselection.ws.client.oscarselector.OscarSelectorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FloristSelectionServiceImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(FloristSelectionServiceImplTest.class);
    private static final double PRICE = 12.12;
    @Autowired
    private AppConfig appConfig;
    @Mock
    private MemberService memberService;
    @Mock
    private OrderSearchService orderSearchService;
    @Mock
    private AvailabilityService availabilityService;
    @Mock
    private SourceCodeRepository sourceCodeRepository;
    @Mock
    private OscarSelectorService oscarSelectorService;
    private FloristSelectionServiceImpl floristSelectionService;
    private FloristSelectionApiRequest floristSelectionApiRequest;
    private FloristSelectionApiRequest.Product product;
    private FloristSelectionApiRequest.Recipient recipient;
    private FloristSelectionApiRequest.RecipientAddress recipientAddress;

    @Before
    public void setUp() {

        floristSelectionService = new FloristSelectionServiceImpl(appConfig, memberService,
                orderSearchService, availabilityService, sourceCodeRepository, oscarSelectorService);

        floristSelectionApiRequest = new FloristSelectionApiRequest();
        product = new FloristSelectionApiRequest.Product();
        recipient = new FloristSelectionApiRequest.Recipient();
        recipientAddress = new FloristSelectionApiRequest.RecipientAddress();

        product.setCode("1212A");
        product.setPrice(BigDecimal.valueOf(PRICE));

        recipientAddress.setAddressLine1("3113 Woodcreek Dr");
        recipientAddress.setPostalCode("60515");
        recipient.setAddress(recipientAddress);

        floristSelectionApiRequest.setDeliveryDate(LocalDate.now());
        floristSelectionApiRequest.setDeliveryDateEnd(LocalDate.now().plusDays(1));
        floristSelectionApiRequest.setProduct(product);
        floristSelectionApiRequest.setExcludedFlorists(new ArrayList<>());
        floristSelectionApiRequest.setRecipient(recipient);
        floristSelectionApiRequest.setSourceCode("1212A");
    }

    @Test
    public void testSelectFillingFlorist() {

        try {
            //Arrange
            List<String> mockFlorists = new ArrayList<>();
            mockFlorists.add("12-1212AA");
            mockFlorists.add("12-1312AA");

            Mono<SourceCodeInfo> mockSourceCodeInfo = Mono.just(SourceCodeInfo.builder().sourceCode("1212A")
                    .oscarScenarioGroup("Test")
                    .primaryFlorists(mockFlorists)
                    .secondaryFlorists(mockFlorists)
                    .preferredFlorists(mockFlorists)
                    .build());

            List<OrderQueryResponse.DeliveryGroup> mockDeliveryGroups = new ArrayList<>();
            mockDeliveryGroups.add(OrderQueryResponse.DeliveryGroup.builder()
                    .executingMemberNo("12-1312AA").fulfillingMember("12-1312AA").build());

            List<OrderQueryResponse.Order> mockOrders = new ArrayList<>();
            mockOrders.add(OrderQueryResponse.Order.builder().deliveryGroups(mockDeliveryGroups).build());

            Flux<AvailabilityInfoResponse> mockResponse = Flux.create(availabilityInfoResponseFluxSink ->
                    availabilityInfoResponseFluxSink.next(AvailabilityInfoResponse.builder()
                            .deliveryDate(LocalDate.now())
                            .availableFlorists(mockFlorists).build()));
            Mono<OrderQueryResponse> mockOrderQueryResponse = Mono.just(OrderQueryResponse.builder()
                    .orderList(mockOrders).build());

            when(availabilityService.getAvailableFlorists(Mockito.anyString(), Mockito.anyString(),
                    Mockito.any(), Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(mockResponse);
            when(orderSearchService.getSameDaySameRecipientFlorists(Mockito.any())).thenReturn(mockOrderQueryResponse);

            when(sourceCodeRepository.get(Mockito.anyString())).thenReturn(mockSourceCodeInfo);

            //Act
            Mono<FloristSelectionApiResponse> responseMono = floristSelectionService.selectFillingFlorist("ftd",
                    floristSelectionApiRequest);

            FloristSelectionApiResponse response = responseMono.block();

            //Assert
            assertThat("12-1312AA" == response.getSelectedMember());
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage());
        }
    }
}
package com.ftd.floristselection;

import com.ftd.floristselection.util.FloristSelectionUtil;
import com.google.cloud.pubsub.v1.Subscriber;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication(scanBasePackages = {"com.ftd.commons", "com.ftd.floristselection"})
public class FloristSelectionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FloristSelectionServiceApplication.class, args);
    }

    @EventListener
    public void startSubscribers(ApplicationReadyEvent applicationReadyEvent) {
        Subscriber sourceCodeSubscriber = (Subscriber) applicationReadyEvent.getApplicationContext()
                .getBean("sourceCodeSubscriber");
        sourceCodeSubscriber.startAsync().awaitRunning();
        FloristSelectionUtil.consumerListener(sourceCodeSubscriber);
    }

}

package com.ftd.floristselection.exception;

/**
 * @author avaka
 */
public class FloristSelectionServiceException extends Exception {
    private static final long serialVersionUID = 1L;

    public FloristSelectionServiceException() {
        super();
    }

    public FloristSelectionServiceException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public FloristSelectionServiceException(String arg0) {
        super(arg0);
    }
}

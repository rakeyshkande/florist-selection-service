package com.ftd.floristselection.pubsub.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.floristselection.api.entity.SourceCodeInfo;
import com.ftd.floristselection.pubsub.SourceCodeChangePubSubMessage;
import com.ftd.floristselection.repository.SourceCodeRepository;
import com.ftd.floristselection.util.PubSubUtil;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.pubsub.v1.PubsubMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Component
@Qualifier("sourceCodeChangeReceiver")
public class SourceCodeReceiver implements MessageReceiver {

    private static final Logger LOG = LoggerFactory.getLogger(SourceCodeReceiver.class);
    private SourceCodeRepository sourceCodeRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public SourceCodeReceiver(SourceCodeRepository sourceCodeRepository,
                              ObjectMapper objectMapper) {
        this.sourceCodeRepository = sourceCodeRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public void receiveMessage(PubsubMessage message, AckReplyConsumer consumer) {

        String messageId = message.getMessageId();
        String data = message.getData().toStringUtf8();
        LOG.info("## received event: message_receiver : {}, message_id : {}, message_data : {}", "sourceCodeReceiver",
                messageId, data);

        performAction(message.getMessageId(), message.getData().toStringUtf8(), consumer);

    }

    public void performAction(String messageId, String messageData, AckReplyConsumer consumer) {

        try {
            SourceCodeChangePubSubMessage message = objectMapper.readValue(messageData,
                    SourceCodeChangePubSubMessage.class);

            sourceCodeRepository.get(message.getSourceCode())
                    .switchIfEmpty(Mono.just(SourceCodeInfo
                            .builder()
                            .sourceCode(message.getSourceCode())
                            .timestamp(LocalDateTime.MIN).build()))
                    .flatMap(sourceCodeInfo -> {
//                        LOG.info("####{}", sourceCodeInfo);
                        if (sourceCodeInfo.getTimestamp().isBefore(message.getTimestamp())) {
                            Mono<SourceCodeInfo> save = sourceCodeRepository.save(PubSubUtil.buildSourceCode(message));
                            consumer.ack();
                            return save;
                        } else {
                            LOG.warn("## received expired event, message_id : {}, source_code:{}",
                                    messageId,
                                    sourceCodeInfo.getSourceCode());
                            Mono<SourceCodeInfo> expired = Mono.just(sourceCodeInfo);
                            consumer.ack();
                            return expired;
                        }
                    })
                    .subscribe();
        } catch (Exception e) {
            LOG.error("error occurred while update the source code info {}", e.getLocalizedMessage());
        }
    }
}


package com.ftd.floristselection.pubsub;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SourceCodeChangePubSubMessage {

    private String sourceCode;
    private String channel;
    private String changeType;
    private String oscarScenarioGroupName;
    private String oscarSelectionEnabledFlag;
    private String primaryBackupRWDFlag;
    private LocalDateTime timestamp;
    private Preferences preferences;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Preferences {
        private List<Member> primaryFloristForSourceCode;
        private List<Member> secondaryFloristsForSourceCode;
        private List<Member> preferredFloristsForSourceCode;
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Member {
        private String floristId;
    }
}
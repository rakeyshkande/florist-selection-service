package com.ftd.floristselection.constants;

public final class CommonConstants {

    public static final String FS_SERVICE = "fss";
    public static final String POS = "POS";
    public static final String FLORIST_FEATURE = "FloristFeature";
    public static final String GOTO_FLORIST = "GOTO Florist";
    public static final int FROM = 0;
    public static final int PAGE_SIZE = 10;
    public static final String FULFILLING_MEMBER = "deliveryGroups.fulfillingMember";
    public static final String DELIVERY_DATE_QUERY_FIELD = "deliveryGroups.deliveryDate";
    public static final String ADDRESS_QUERY_FIELD = "deliveryGroups.recipientDetails.address1";
    public static final String ZIP_CODE_QUERY_FIELD = "deliveryGroups.recipientDetails.postalCode";
    public static final String STATUS_QUERY_FIELD = "deliveryGroups.orderStatus";
    public static final String FULFILLMENT_CHANNEL_QUERY_FIELD = "deliveryGroups.lineItems.fulfillmentChannel";
    public static final String WITH_FLORIST = "WITH_FLORIST";
    public static final String FLORIST_FULFILLMENT = "florist";
    public static final String ORDER_TIMESTAMP = "orderTimestamp";


    public static final String NO_FLORIST_FOUND = "No florists found with selected criteria";

    private CommonConstants() {

    }
}

package com.ftd.floristselection.constants;

public enum Collection {
    SOURCE_CODE_INFO(CollectionName.SOURCE_CODE_INFO);
    private final String value;

    Collection(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static final class CollectionName {
        private CollectionName() {

        }

        public static final String SOURCE_CODE_INFO = "source-code-info";
    }
}

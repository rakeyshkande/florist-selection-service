package com.ftd.floristselection.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author avaka
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FloristSelectionApiRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private String masterOrderNo;
    private String lineOrderNo;
    private Product product;
    private Recipient recipient;
    private LocalDate deliveryDate;
    private LocalDate deliveryDateEnd;
    private String sourceCode;
    private String scenarioGroup;
    private String sendingMemberCode;
    private List<String> excludedFlorists;

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Product implements Serializable {
        private static final long serialVersionUID = 1L;
        private String code;
        private BigDecimal price;
        private String alternateProductCode;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Recipient implements Serializable {
        private static final long serialVersionUID = 1L;
        private String title;
        private String firstName;
        private String lastName;
        private String middleName;
        private String timeOfService;
        private RecipientAddress address;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RecipientAddress implements Serializable {
        private static final long serialVersionUID = 1L;
        private String addressLine1;
        private String addressLine2;
        private String cityName;
        private String stateCode;
        private String stateName;
        private String postalCode;
        private String countryCode;
        private String countryName;
        private String countryCodeISO;
        private AddressType addressType;
        private GeoCode geoCode;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddressType implements Serializable {
        private static final long serialVersionUID = 1L;
        private String type;
        private String name;
        private String info;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GeoCode implements Serializable {
        private static final long serialVersionUID = 1L;
        private BigDecimal latitude;
        private BigDecimal longitude;
    }
}

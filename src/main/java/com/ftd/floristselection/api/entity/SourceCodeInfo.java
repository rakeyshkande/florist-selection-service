package com.ftd.floristselection.api.entity;

import com.ftd.floristselection.constants.Collection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author avaka
 */
@AllArgsConstructor
@Builder
@Data
@ToString
@Document(collection = Collection.CollectionName.SOURCE_CODE_INFO)
public class SourceCodeInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String sourceCode;

    private String channel;
    private String changeType;
    private String oscarScenarioGroup;
    private Boolean oscarSelectionEnabledFlag;
    private Boolean primaryBackupRWDFlag;

    private List<String> primaryFlorists;
    private List<String> secondaryFlorists;
    private List<String> preferredFlorists;
    private LocalDateTime timestamp;

}

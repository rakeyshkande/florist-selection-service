package com.ftd.floristselection.api;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author avaka
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FloristSelectionFact implements Serializable {
    private static final long serialVersionUID = 1L;
    private String sourceCode;
    private List<FloristData> floristDataList;

    private List<String> filteredFlorists;
}

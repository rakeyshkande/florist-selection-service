package com.ftd.floristselection.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author avaka
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FloristSelectionApiResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String selectedMember;
    private LocalDate deliveryDate;
}

package com.ftd.floristselection.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author avaka
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FloristData {
    private String id;
    private String memberCode;
    private String memberNumber;
    private Boolean isGoto;
    private Boolean hasMercury;
}

package com.ftd.floristselection.config;

import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.core.GoogleCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
@Configuration
@EnableConfigurationProperties({AppConfigProperties.class, GCPProperties.class})
public class AppConfig {

    private static final List<String> CREDENTIALS_SCOPE = Collections.unmodifiableList(
            Arrays.asList("https://www.googleapis.com/auth/pubsub"));

    @Autowired
    private AppConfigProperties appConfigProperties;

    @Bean
    @ConditionalOnMissingBean
    public CredentialsProvider credentialProvider(
            @Value("${spring.cloud.gcp.credentials.location}") Resource credentialLocation) throws IOException {
        CredentialsProvider credentialsProvider = null;
        if (credentialLocation != null && credentialLocation.exists()) {
            credentialsProvider = FixedCredentialsProvider.create(
                    GoogleCredentials.fromStream(credentialLocation.getInputStream()).createScoped(CREDENTIALS_SCOPE));
        } else {
            credentialsProvider = GoogleCredentialsProvider.newBuilder().setScopesToApply(CREDENTIALS_SCOPE).build();
        }
        return credentialsProvider;
    }
}

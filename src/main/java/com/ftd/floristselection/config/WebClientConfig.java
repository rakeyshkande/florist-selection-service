package com.ftd.floristselection.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelOption;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.handler.timeout.ReadTimeoutHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.resources.PoolResources;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author avaka
 */
@Configuration
@EnableConfigurationProperties({OscarSelectorWSProperties.class, MemberWSProperties.class,
        FloristAvailabilityWSProperties.class, OrderSearchWSProperties.class})
public class WebClientConfig {
    private ObjectMapper objectMapper;
    private MemberWSProperties memberWSProperties;
    private OrderSearchWSProperties orderSearchWSProperties;
    private OscarSelectorWSProperties oscarSelectorWSProperties;
    private FloristAvailabilityWSProperties floristAvailabilityWSProperties;
    private static final Logger LOGGER = LoggerFactory.getLogger(WebClientConfig.class);

    @Autowired
    public WebClientConfig(ObjectMapper objectMapper,
                           MemberWSProperties memberWSProperties,
                           OrderSearchWSProperties orderSearchWSProperties,
                           OscarSelectorWSProperties oscarSelectorWSProperties,
                           FloristAvailabilityWSProperties floristAvailabilityWSProperties) {
        this.objectMapper = objectMapper;
        this.memberWSProperties = memberWSProperties;
        this.orderSearchWSProperties = orderSearchWSProperties;
        this.oscarSelectorWSProperties = oscarSelectorWSProperties;
        this.floristAvailabilityWSProperties = floristAvailabilityWSProperties;
    }

    @Bean(name = "memberWebClient")
    public WebClient memberWebClient() {
        return getWebClient(memberWSProperties, "member-service");
    }

    @Bean(name = "orderSearchWebClient")
    public WebClient orderSearchWebClient() {
        return getWebClient(orderSearchWSProperties, "order-search-service");
    }

    @Bean(name = "floristAvailabilityWebClient")
    public WebClient floristAvailabilityWebClient() {
        return getWebClient(floristAvailabilityWSProperties, "florist-availability-service");
    }

    private WebClient getWebClient(WSProperties wsProperties, String client) {
        return WebClient.builder().baseUrl(wsProperties.getUri())
                .clientConnector(getReactorClientHttpConnector(getSslContext(), wsProperties, client))
                .exchangeStrategies(getExchangeStrategies())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .filter(logRequest())
//                .filter(logResponseStatus())
                .build();
    }

    private Integer getTimeOut(WSProperties wsProperties) {
        RetryableProperties retryableProperties = wsProperties.getRetryableProperties();
        if (Objects.nonNull(retryableProperties) && retryableProperties.getNumRetries() > 0) {
            return retryableProperties.getTimeout();
        }
        return wsProperties.getTimeout();
    }

    private SslContext getSslContext() {
        SslContext sslContext = null;
        try {
            sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
        } catch (Exception e) {
            LOGGER.warn("{}", "exception occurred while creating ssl-context", e);
        }
        return sslContext;
    }

    //deprecated in higher versions of Spring
    private ReactorClientHttpConnector getReactorClientHttpConnector(
            SslContext sslContext, WSProperties wsProperties, String client) {
        return new ReactorClientHttpConnector(
                options ->
                        options.sslContext(sslContext)
                                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, getTimeOut(wsProperties))
                                .poolResources(PoolResources.fixed("fss-http-" + client,
                                        wsProperties.getPoolResource().getMaxConnections(),
                                        wsProperties.getPoolResource().getAcquireTimeout()))
                                .compression(true)
                                .afterNettyContextInit(ctx -> {
                                    ctx.addHandlerLast(new ReadTimeoutHandler(getTimeOut(wsProperties), TimeUnit.MILLISECONDS));
                                }));
    }

    private ExchangeStrategies getExchangeStrategies() {
        return ExchangeStrategies.builder()
                .codecs(clientCodecConfigurer -> {
                    clientCodecConfigurer.defaultCodecs()
                            .jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON));
                }).build();
    }

    private ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            List<String> headersList = new ArrayList<>();
            clientRequest.headers()
                    .forEach((name, values) -> values.forEach(value ->
                            headersList.add(new StringBuilder(name).append("=").append(value).toString())));
            LOGGER.info("#Request: #HttpMethod: {} #Url: {} #Headers: {}", clientRequest.method(), clientRequest.url(), headersList.toString());
            return Mono.just(clientRequest);
        });
    }

    private ExchangeFilterFunction logResponseStatus() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            LOGGER.info("#Response: #HttpStatus {}", clientResponse.statusCode());
            return Mono.just(clientResponse);
        });
    }

    @Bean(name = "oscarSelectorWSTemplate")
    public WebServiceTemplate pasWSTemplate() {
        WebServiceTemplate template = new WebServiceTemplate();

        HttpComponentsMessageSender messageSender = new HttpComponentsMessageSender();
        messageSender.setReadTimeout(Integer.valueOf(oscarSelectorWSProperties.getTimeout()));
        messageSender.setConnectionTimeout(Integer.valueOf(oscarSelectorWSProperties.getTimeout()));

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(oscarSelectorWSProperties.getJaxbContextPath());

        template.setMarshaller(marshaller);
        template.setUnmarshaller(marshaller);
        template.setDefaultUri(oscarSelectorWSProperties.getUri());
        template.setMessageSender(messageSender);

        return template;
    }

}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.oscar-selector.service")
class OscarSelectorWSProperties extends WSProperties {
    private String jaxbContextPath;
}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.member.service")
class MemberWSProperties extends WSProperties {

}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.florist-availability.service")
class FloristAvailabilityWSProperties extends WSProperties {

}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.order-search.service")
class OrderSearchWSProperties extends WSProperties {

}

@Data
class WSProperties {
    private String uri;
    private Integer timeout;
    private PoolResource poolResource;
    private RetryableProperties retryableProperties;
}

@Data
class RetryableProperties {
    private Integer numRetries;
    private Integer timeout;
}

@Data
class PoolResource {
    private Integer maxConnections;
    private Integer acquireTimeout;
}

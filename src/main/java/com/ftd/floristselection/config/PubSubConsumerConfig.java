package com.ftd.floristselection.config;

import com.ftd.floristselection.pubsub.receiver.SourceCodeReceiver;
import com.google.api.gax.core.CredentialsProvider;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class PubSubConsumerConfig {

    private AppConfig appConfig;
    private GCPProperties gcpProperties;
    private CredentialsProvider credentialProvider;
    private static final Logger LOG = LoggerFactory.getLogger(PubSubConsumerConfig.class);

    @Autowired
    public PubSubConsumerConfig(AppConfig appConfig,
                                GCPProperties gcpProperties,
                                CredentialsProvider credentialProvider) {
        this.appConfig = appConfig;
        this.gcpProperties = gcpProperties;
        this.credentialProvider = credentialProvider;
    }

    public Subscriber.Builder createSubscriber(String subscription, MessageReceiver messageReceiver) {
        LOG.info("projectId: {}, subscription: {}", gcpProperties.getPubSub().getProjectId(), subscription);
        ProjectSubscriptionName projectSubscriptionName =
                ProjectSubscriptionName.of(gcpProperties.getPubSub().getProjectId(),
                        subscription);
        return Subscriber.newBuilder(projectSubscriptionName, messageReceiver).setCredentialsProvider(credentialProvider);
    }

    @Bean("sourceCodeSubscriber")
    public Subscriber getSourceCodeSubscriber(SourceCodeReceiver sourceCodeReceiver) {
        Subscriber.Builder builder = createSubscriber(appConfig.getAppConfigProperties().getSourceCodeSubscription(),
                sourceCodeReceiver);
        return builder.build();
    }

}

package com.ftd.floristselection.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "properties.florist-selection.service")
public class AppConfigProperties {
    private String drlFile;
    private String sourceCodeSubscription;
    private Defaults defaults;

    @Data
    public static class Defaults {
        private String sourceCode;
        private String scenarioGroup;
    }
}
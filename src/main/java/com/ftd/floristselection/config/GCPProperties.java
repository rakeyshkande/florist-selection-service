package com.ftd.floristselection.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@ConfigurationProperties(prefix = "spring.cloud.gcp")
public class GCPProperties {
    private PubSub pubSub;

    @Data
    public static class PubSub {
        private String projectId;
        private String nameSpace;
    }

}


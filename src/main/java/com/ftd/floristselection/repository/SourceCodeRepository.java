package com.ftd.floristselection.repository;

import com.ftd.floristselection.api.entity.SourceCodeInfo;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * @author avaka
 */
@Repository
public interface SourceCodeRepository {
    Mono<SourceCodeInfo> get(String sourceCode);

    Mono<SourceCodeInfo> save(SourceCodeInfo sourceCodeInfo);

    Mono<SourceCodeInfo> save(Mono<SourceCodeInfo> sourceCodeInfoMono);
}

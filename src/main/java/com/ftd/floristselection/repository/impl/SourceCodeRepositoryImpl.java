package com.ftd.floristselection.repository.impl;

import com.ftd.floristselection.api.entity.SourceCodeInfo;
import com.ftd.floristselection.repository.SourceCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * @author avaka
 */
@Component
public class SourceCodeRepositoryImpl implements SourceCodeRepository {

    private ReactiveMongoTemplate reactiveMongoTemplate;

    @Autowired
    public SourceCodeRepositoryImpl(ReactiveMongoTemplate reactiveMongoTemplate) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
    }

    @Override
    public Mono<SourceCodeInfo> get(String sourceCode) {
        return reactiveMongoTemplate.findById(sourceCode, SourceCodeInfo.class);
    }

    @Override
    public Mono<SourceCodeInfo> save(SourceCodeInfo sourceCodeInfo) {
        return reactiveMongoTemplate.save(sourceCodeInfo);
    }

    @Override
    public Mono<SourceCodeInfo> save(Mono<SourceCodeInfo> sourceCodeInfoMono) {
        return reactiveMongoTemplate.save(sourceCodeInfoMono);
    }
}

package com.ftd.floristselection.ws.client.floristavailability.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @author avaka
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AvailabilityInfoResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("deliveryDate")
    private LocalDate deliveryDate;
    @JsonProperty("floristId")
    private List<String> availableFlorists;
}

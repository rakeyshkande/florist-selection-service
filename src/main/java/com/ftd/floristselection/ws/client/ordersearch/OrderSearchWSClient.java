package com.ftd.floristselection.ws.client.ordersearch;

import com.ftd.floristselection.ws.client.WSClientUtil;
import com.ftd.floristselection.ws.client.WSException;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryRequest;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.util.logging.Level;


/**
 * @author makhtar
 */
@Component
public class OrderSearchWSClient {
    private static final Logger LOG = LoggerFactory.getLogger(OrderSearchWSClient.class);
    private static final String URI_ORDER_SEARCH = "/order/queryByFields";
    private WebClient webClient;
    @Value("${properties.order-search.service.retryable-properties.numRetries}")
    private Integer numRetries;

    @Autowired
    public OrderSearchWSClient(@Qualifier("orderSearchWebClient") WebClient orderSearchWebClient) {
        this.webClient = orderSearchWebClient;
    }

    public Mono<OrderQueryResponse> getSameDaySameRecipientFlorists(OrderQueryRequest orderQueryRequest) {
        return webClient.post()
                .uri(URI_ORDER_SEARCH)
                .body(Mono.just(orderQueryRequest), OrderQueryRequest.class)
                .exchange()
                .flatMap(WSClientUtil::handleOrderSearchResponse)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getSameDaySameRecipientFlorists",
                                orderQueryRequest))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getSameDaySameRecipientFlorists",
                                orderQueryRequest))
                .onErrorMap(WSException.class, ex -> ex)
                .log("order-search.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_NEXT,
                        SignalType.ON_COMPLETE);
    }
}

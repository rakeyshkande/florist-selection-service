package com.ftd.floristselection.ws.client.ordersearch.domain;

import com.ftd.floristselection.constants.CommonConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderQueryRequest {

    @Builder.Default
    private int pageSize = CommonConstants.PAGE_SIZE;
    @Builder.Default
    private int offset = CommonConstants.FROM;
    @Singular
    private List<String> includedFields;
    @Singular
    private List<QueryField> queryFields;
    private List<QueryField> filterQueryFields;
    private Sort sort;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class QueryField {
        private String[] fields;
        private String pattern;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Sort {
        private String fieldName;
        private SortOrder sortOrder;
    }

    public enum SortOrder {
        desc, asc;
    }
}


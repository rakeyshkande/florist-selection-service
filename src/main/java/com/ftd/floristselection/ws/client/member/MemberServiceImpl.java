package com.ftd.floristselection.ws.client.member;


import com.ftd.floristselection.ws.client.member.domain.IdentityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.MemberInfoResponse;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.circuitbreaker.autoconfigure.CircuitBreakerProperties;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author avaka
 */
@Component
public class MemberServiceImpl implements MemberService {
    private MemberWSClient memberWSClient;
    private final CircuitBreaker circuitBreaker;

    private static final Logger LOG = LoggerFactory.getLogger(MemberServiceImpl.class);

    @Autowired
    public MemberServiceImpl(MemberWSClient memberWSClient, CircuitBreakerRegistry circuitBreakerRegistry,
                             CircuitBreakerProperties circuitBreakerProperties) {
        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker(
                BACKEND,
                () -> circuitBreakerProperties
                        .createCircuitBreakerConfig(BACKEND));
        this.memberWSClient = memberWSClient;
    }

    @Override
    public Flux<IdentityInfoResponse> getIdentityInfo(String siteId, List<String> memberCodes) {
        return memberWSClient.getIdentityInfo(siteId, memberCodes)
                .transform(CircuitBreakerOperator.of(circuitBreaker));
    }

    @Override
    public Mono<MemberInfoResponse> getMemberInfo(String siteId, String memberId) {
        return memberWSClient.getMemberInfo(siteId, memberId)
                .transform(CircuitBreakerOperator.of(circuitBreaker))
                .onErrorReturn(throwable -> {
                    LOG.error("#getMemberInfo: {}", throwable);
                    return true;
                }, getMemberInfoFallbackResponse(memberId));
    }

    private MemberInfoResponse getMemberInfoFallbackResponse(String memberId) {
        return MemberInfoResponse.builder().id(memberId).identity(null).memberInfo(null).subscriptionInfo(null).build();
    }
}

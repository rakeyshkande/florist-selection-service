package com.ftd.floristselection.ws.client.member.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author avaka
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MemberInfoResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private IdentityInfoResponse.Identity identity;
    private MemberInfo memberInfo;
    private SubscriptionInfo subscriptionInfo;

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MemberInfo implements Serializable {
        private static final long serialVersionUID = 1L;
        private BasicInfo basicInfo;
        private List<Rating> ratings;
        private List<Preference> preferences;
        private AdvancedInfo advancedInfo;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BasicInfo implements Serializable {
        private static final long serialVersionUID = 1L;
        private String type;
        private String subType;
        private String brand;
        private String ownerName;
        private String businessName;
        private String internalLink;
        private String parentCode;
        private String parentMemberId;
        private String standaloneBranch;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Preference implements Serializable {
        private static final long serialVersionUID = 1L;
        private String name;
        private List<String> value;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Rating implements Serializable {
        private static final long serialVersionUID = 1L;
        private String id;
        private String type;
        private String cycle;
        private Boolean starEarned;
        private Boolean starEligible;
        private BigDecimal ratingPercentage;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AdvancedInfo implements Serializable {
        private static final long serialVersionUID = 1L;
        private float nps;
        private String erosOnline;
        private Boolean isGroceryStore;
        private String apolloMemberStatus;
    }

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SubscriptionInfo implements Serializable {
        private static final long serialVersionUID = 1L;
        private List<Subscription> subscriptions;
    }


    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Subscription implements Serializable {
        private static final long serialVersionUID = 1L;
        private String id;
        private String name;
        private String type;
        private String version;
        private Date validTo;
        private Date validFrom;
        private String description;
    }

}

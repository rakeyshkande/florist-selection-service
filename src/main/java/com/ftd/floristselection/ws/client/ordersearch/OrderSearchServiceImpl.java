package com.ftd.floristselection.ws.client.ordersearch;

import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryRequest;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.circuitbreaker.autoconfigure.CircuitBreakerProperties;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class OrderSearchServiceImpl implements OrderSearchService {

    private final CircuitBreaker circuitBreaker;
    private OrderSearchWSClient orderSearchWebClient;

    private static final Logger LOG = LoggerFactory.getLogger(OrderSearchServiceImpl.class);

    @Autowired
    public OrderSearchServiceImpl(OrderSearchWSClient orderSearchWebClient,
                                  CircuitBreakerRegistry circuitBreakerRegistry,
                                  CircuitBreakerProperties circuitBreakerProperties) {
        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker(
                BACKEND,
                () -> circuitBreakerProperties
                        .createCircuitBreakerConfig(BACKEND));
        this.orderSearchWebClient = orderSearchWebClient;
    }

    @Override
    public Mono<OrderQueryResponse> getSameDaySameRecipientFlorists(OrderQueryRequest orderQueryRequest) {
        return orderSearchWebClient.getSameDaySameRecipientFlorists(orderQueryRequest)
                .transform(CircuitBreakerOperator.of(circuitBreaker))
                .onErrorReturn(throwable -> {
                    LOG.error("#searchOrderForSameDayFlorist: {}", throwable);
                    return true;
                }, getSameDayFloristFallbackResponse());
    }

    private OrderQueryResponse getSameDayFloristFallbackResponse() {
        return OrderQueryResponse.builder().orderList(null).build();
    }


}

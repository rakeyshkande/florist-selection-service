package com.ftd.floristselection.ws.client.oscarselector;

import com.ftd.floristselection.ws.client.WSClientException;
import com.ftd.floristselection.ws.client.WSException;
import com.ftd.oscar.athena.selector.SelectFillingMember;
import com.ftd.oscar.athena.selector.SelectFillingMemberResponse;
import org.springframework.stereotype.Service;

@Service
public interface OscarSelectorService {
    SelectFillingMemberResponse selectFillingMember(SelectFillingMember fillingMember) throws WSException, WSClientException;
}



package com.ftd.floristselection.ws.client.ordersearch.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderQueryResponse {

    private int pageSize;
    private int offset;
    private List<Order> orderList;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Order implements Serializable {
        private List<DeliveryGroup> deliveryGroups;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeliveryGroup {
        private String fulfillingMember;
        private String executingMemberNo;
        private String deliveryGroupId;
    }
}

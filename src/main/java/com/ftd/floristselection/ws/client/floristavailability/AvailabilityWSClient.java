package com.ftd.floristselection.ws.client.floristavailability;

import com.ftd.floristselection.ws.client.WSClientUtil;
import com.ftd.floristselection.ws.client.WSException;
import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.SignalType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.logging.Level;

/**
 * @author avaka
 */
@Component
public class AvailabilityWSClient {

    private WebClient webClient;
    private static final Logger LOG = LoggerFactory.getLogger(AvailabilityWSClient.class);
    private static final String URI_GET_AVAILABLE_FLORISTS = "/{siteId}/api/floristavailabilities";

    @Value("${properties.florist-availability.service.retryable-properties.numRetries}")
    private Integer numRetries;

    @Autowired
    public AvailabilityWSClient(@Qualifier("floristAvailabilityWebClient") WebClient floristAvailabilityWebClient) {
        this.webClient = floristAvailabilityWebClient;
    }

    public Flux<AvailabilityInfoResponse> getAvailableFlorists(String siteId, String productId, BigDecimal productPrice,
                                                               String zipCode, LocalDate deliveryDate, LocalDate deliveryDateEnd) {
        return webClient
                .get()
                .uri(uriBuilder -> WSClientUtil.buildAvailableFloristsURI(uriBuilder, URI_GET_AVAILABLE_FLORISTS, siteId,
                        productId, productPrice, zipCode, deliveryDate, deliveryDateEnd))
                .exchange()
                .flatMap(WSClientUtil::handleAvailableFloristResponse)
                .flatMapIterable(WSClientUtil::availabilityInfo)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getAvailableFlorists",
                                WSClientUtil.availabilityInfoData(productId, zipCode, deliveryDate)))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getAvailableFlorists",
                                WSClientUtil.availabilityInfoData(productId, zipCode, deliveryDate)))
                .onErrorMap(WSException.class, ex -> ex)
                .log("availability.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_NEXT,
                        SignalType.ON_COMPLETE);
    }
}

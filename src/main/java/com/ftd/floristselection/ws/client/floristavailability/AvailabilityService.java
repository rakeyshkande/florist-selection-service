package com.ftd.floristselection.ws.client.floristavailability;

import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author avaka
 */
@Service
public interface AvailabilityService {
    String BACKEND = "florist-availability-service"; //NOSONAR

    Flux<AvailabilityInfoResponse> getAvailableFlorists(String siteId, String productId, BigDecimal productPrice,
                                                        String zipCode, LocalDate deliveryDate, LocalDate deliveryDateEnd);
}

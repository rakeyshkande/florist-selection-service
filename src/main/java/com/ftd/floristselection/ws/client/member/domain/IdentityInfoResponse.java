package com.ftd.floristselection.ws.client.member.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author avaka
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class IdentityInfoResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private Identity identity;

    @Data
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Identity implements Serializable {
        private static final long serialVersionUID = 1L;
        private String source;
        private String memberId;
        private String memberCode;
    }
}




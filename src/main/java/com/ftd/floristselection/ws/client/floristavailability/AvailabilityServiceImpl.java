package com.ftd.floristselection.ws.client.floristavailability;

import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.circuitbreaker.autoconfigure.CircuitBreakerProperties;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author avaka
 */
@Component
public class AvailabilityServiceImpl implements AvailabilityService {

    private final CircuitBreaker circuitBreaker;
    private AvailabilityWSClient availabilityWSClient;

    private static final Logger LOG = LoggerFactory.getLogger(AvailabilityServiceImpl.class);

    @Autowired
    public AvailabilityServiceImpl(AvailabilityWSClient availabilityWSClient, CircuitBreakerRegistry circuitBreakerRegistry,
                                   CircuitBreakerProperties circuitBreakerProperties) {
        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker(
                BACKEND,
                () -> circuitBreakerProperties
                        .createCircuitBreakerConfig(BACKEND));
        this.availabilityWSClient = availabilityWSClient;
    }

    @Override
    public Flux<AvailabilityInfoResponse> getAvailableFlorists(String siteId, String productId, BigDecimal productPrice,
                                                               String zipCode, LocalDate deliveryDate, LocalDate deliveryDateEnd) {
        return availabilityWSClient.getAvailableFlorists(siteId, productId, productPrice, zipCode, deliveryDate, deliveryDateEnd)
                .transform(CircuitBreakerOperator.of(circuitBreaker))
                .onErrorMap(throwable -> {
                    LOG.error("#getAvailableFlorists: {}", throwable);
                    return throwable;
                });
    }
}

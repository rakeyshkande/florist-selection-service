package com.ftd.floristselection.ws.client.member;

import com.ftd.floristselection.ws.client.WSClientUtil;
import com.ftd.floristselection.ws.client.WSException;
import com.ftd.floristselection.ws.client.member.domain.IdentityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.MemberInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.util.List;
import java.util.logging.Level;


/**
 * @author avaka
 */
@Component
public class MemberWSClient {
    private WebClient webClient;
    private static final Logger LOG = LoggerFactory.getLogger(MemberWSClient.class);
    private static final String URI_IDENTITY_INFO = "/{siteId}/members/identity-info";
    private static final String URI_MEMBER_INFO = "/{siteId}/members/{memberId}/member-subscription-info";

    @Value("${properties.member.service.retryable-properties.numRetries}")
    private Integer numRetries;

    @Autowired
    public MemberWSClient(@Qualifier("memberWebClient") WebClient memberWebClient) {
        this.webClient = memberWebClient;
    }

    public Flux<IdentityInfoResponse> getIdentityInfo(String siteId, List<String> memberCodes) {
        return webClient
                .get()
                .uri(uriBuilder -> WSClientUtil.buildIdentityInfoURI(uriBuilder, URI_IDENTITY_INFO, siteId, memberCodes))
                .exchange()
                .flatMap(WSClientUtil::handleIdentityInfoResponse)
                .flatMapIterable(WSClientUtil::identityInfo)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getIdentityInfo", memberCodes))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getIdentityInfo", memberCodes))
                .onErrorMap(WSException.class, ex -> ex)
                .log("member-identity.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_NEXT,
                        SignalType.ON_COMPLETE);
    }

    public Mono<MemberInfoResponse> getMemberInfo(String siteId, String memberId) {
        return webClient.get()
                .uri(URI_MEMBER_INFO, siteId, memberId)
                .retrieve()
                .bodyToMono(MemberInfoResponse.class)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getMemberInfo", memberId))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getMemberInfo", memberId))
                .onErrorMap(WSException.class, ex -> ex)
                .log("member-info.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_COMPLETE);
    }

}

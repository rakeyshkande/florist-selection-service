package com.ftd.floristselection.ws.client.member;

import com.ftd.floristselection.ws.client.member.domain.IdentityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.MemberInfoResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author avaka
 */
@Service
public interface MemberService {
    String BACKEND = "member-service"; //NOSONAR
    Flux<IdentityInfoResponse> getIdentityInfo(String siteId, List<String> memberCodes);

    Mono<MemberInfoResponse> getMemberInfo(String siteId, String memberId);
}

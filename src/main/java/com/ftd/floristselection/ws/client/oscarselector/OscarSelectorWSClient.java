package com.ftd.floristselection.ws.client.oscarselector;

import com.ftd.floristselection.ws.client.WSClientException;
import com.ftd.floristselection.ws.client.WSException;
import com.ftd.oscar.athena.selector.ObjectFactory;
import com.ftd.oscar.athena.selector.SelectFillingMember;
import com.ftd.oscar.athena.selector.SelectFillingMemberResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import javax.xml.bind.JAXBElement;

/**
 * @author avaka
 */
@Component
public class OscarSelectorWSClient {

    @Autowired
    @Qualifier("oscarSelectorWSTemplate")
    private WebServiceTemplate webServiceTemplate;

    private static final Logger LOG = LoggerFactory.getLogger(OscarSelectorWSClient.class);

    @Retryable(value = WSException.class,
            maxAttemptsExpression = "${properties.oscar-selector.service.retryable.maxAttempts}",
            backoff = @Backoff(delayExpression = "${properties.oscar-selector.service.retryable.backoffMilliseconds}"))
    public SelectFillingMemberResponse selectFillingMember(SelectFillingMember fillingMember) throws WSException, WSClientException {
        SelectFillingMemberResponse fillingMemberResponse = null;
        try {
            @SuppressWarnings("unchecked")
            JAXBElement<SelectFillingMemberResponse> response = (JAXBElement<SelectFillingMemberResponse>) this.webServiceTemplate
                    .marshalSendAndReceive(new ObjectFactory().createSelectFillingMember(fillingMember));
            fillingMemberResponse = response.getValue();
            return fillingMemberResponse;
        } catch (WebServiceIOException ioException) {
            LOG.error(ioException.getMessage());
            throw new WSException(ioException.toString(), ioException);
        } catch (SoapFaultClientException soapFault) {
            StringBuilder error = new StringBuilder(
                    "Exception occurred while calling to Oscar Selector");
            LOG.error(error.toString(), soapFault);
            throw new WSException(error.toString(), soapFault);
        } catch (Exception e) {
            StringBuilder error = new StringBuilder(
                    "Exception occurred while fetching select florist from Oscar Selector");
            LOG.error(error.toString(), e);
            throw new WSClientException(error.toString(), e);
        }

    }

}

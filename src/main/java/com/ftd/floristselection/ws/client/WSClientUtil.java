package com.ftd.floristselection.ws.client;

import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.IdentityInfoResponse;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import io.netty.channel.ConnectTimeoutException;
import io.netty.handler.timeout.ReadTimeoutException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author avaka
 */
public final class WSClientUtil {
    private static final Logger LOG = LoggerFactory.getLogger(WSClientUtil.class);

    private WSClientUtil() {

    }

    public static <T> void log(String method, String errorMsg, T data) {
        LOG.error("method_name: {}, error_msg: {}, data: {}", method, errorMsg, data);
    }

    public static URI buildIdentityInfoURI(UriBuilder uriBuilder, String path, String siteId, List<String> memberCodes) {
        return uriBuilder.path(path)
                .queryParam("memberCodes", StringUtils.join(memberCodes, ","))
                .build(siteId);
    }

    public static WebClientResponseException throwWebClientResponseException(ClientResponse response) {
        String msg = String.format("ClientResponse has erroneous status code: %d %s", response.statusCode().value(),
                response.statusCode().getReasonPhrase());
        WebClientResponseException webClientResponseException = new WebClientResponseException(msg,
                response.statusCode().value(),
                response.statusCode().getReasonPhrase(),
                response.headers().asHttpHeaders(), null, null);
        return webClientResponseException;
    }

    //whenever we throw WSException; retry for 2 more times; change accordingly
    public static Boolean getWSExceptionPredicate(Throwable throwable) {
        return throwable instanceof ConnectTimeoutException || throwable instanceof ReadTimeoutException
                || (throwable instanceof WebClientResponseException && HttpStatus.INTERNAL_SERVER_ERROR.equals(((WebClientResponseException) throwable).getStatusCode()));
    }

    public static Boolean getWSClientExceptionPredicate(Throwable throwable) {
        return !(throwable instanceof WSException);
    }

    public static <T> Throwable throwWSException(Throwable throwable, String method, T data) {
        log(method, throwable.getMessage(), data);
        return new WSException(throwable.getMessage(), throwable);
    }

    public static <T> Throwable throwWSClientException(Throwable throwable, String method, T data) {
        log(method, throwable.getMessage(), data);
        return new WSClientException(throwable.getMessage(), throwable);
    }

    public static Boolean retryPredicate(Throwable throwable, Integer numRetries) {
        return throwable instanceof WSException && numRetries > 0;
    }

    public static Mono<Map<String, List<IdentityInfoResponse>>> handleIdentityInfoResponse(ClientResponse response) {
        if (response.statusCode().isError()) {
            return response.body((clientHttpResponse, context) ->
                    Mono.error(WSClientUtil.throwWebClientResponseException(response))
            );
        }
        return response.bodyToMono(new ParameterizedTypeReference<Map<String, List<IdentityInfoResponse>>>() {
        });
    }

    public static List<IdentityInfoResponse> identityInfo(Map<String, List<IdentityInfoResponse>> identityMap) {
        List<IdentityInfoResponse> identityInfo = new ArrayList<>();
        identityMap.entrySet().forEach(memberCode -> {
            if (CollectionUtils.isNotEmpty(memberCode.getValue())) {
                identityInfo.addAll(memberCode.getValue());
            }
        });
        return identityInfo;
    }

    public static URI buildAvailableFloristsURI(UriBuilder uriBuilder, String path, String siteId,
                                                String productId, BigDecimal productPrice, String zipCode,
                                                LocalDate deliveryDate, LocalDate deliveryDateEnd) {
        return uriBuilder.path(path)
                .queryParam("productId", productId)
                .queryParam("zipCode", zipCode)
                .queryParam("startDate", deliveryDate)
                .queryParam("endDate", deliveryDateEnd)
                .queryParam("productPrice", productPrice)
                .build(siteId);
    }

    public static Mono<List<AvailabilityInfoResponse>> handleAvailableFloristResponse(ClientResponse response) {
        if (response.statusCode().isError()) {
            return response.body((clientHttpResponse, context) ->
                    Mono.error(WSClientUtil.throwWebClientResponseException(response))
            );
        }
        return response.bodyToMono(new ParameterizedTypeReference<List<AvailabilityInfoResponse>>() {
        });
    }

    public static Mono<OrderQueryResponse> handleOrderSearchResponse(ClientResponse response) {
        if (response.statusCode().isError()) {
            return response.body((clientHttpResponse, context) ->
                    Mono.error(WSClientUtil.throwWebClientResponseException(response))
            );
        }
        return response.bodyToMono(new ParameterizedTypeReference<OrderQueryResponse>() {
        });
    }

    public static List<AvailabilityInfoResponse> availabilityInfo(List<AvailabilityInfoResponse> availabilityInfoResponses) {
        return availabilityInfoResponses;
    }

    public static String availabilityInfoData(String productId, String zipCode, LocalDate deliveryDate) {
        return new StringBuilder("productId: ").append(productId).append(" zipCode: ").append(zipCode)
                .append(" deliveryDate: ").append(deliveryDate).toString();
    }
}

package com.ftd.floristselection.ws.client.oscarselector;

import com.ftd.floristselection.ws.client.WSClientException;
import com.ftd.floristselection.ws.client.WSException;
import com.ftd.oscar.athena.selector.SelectFillingMember;
import com.ftd.oscar.athena.selector.SelectFillingMemberResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OscarSelectorServiceImpl implements OscarSelectorService {

    private OscarSelectorWSClient oscarSelectorWSClient;

    @Autowired
    public OscarSelectorServiceImpl(OscarSelectorWSClient oscarSelectorWSClient) {
        this.oscarSelectorWSClient = oscarSelectorWSClient;
    }

    //add hystrix configurations and fall back method
    @Override
    public SelectFillingMemberResponse selectFillingMember(SelectFillingMember fillingMember) throws WSException, WSClientException {
        return oscarSelectorWSClient.selectFillingMember(fillingMember);
    }
}

package com.ftd.floristselection.ws.client.ordersearch;

import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryRequest;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import reactor.core.publisher.Mono;

public interface OrderSearchService {
    String BACKEND = "order-search-service"; //NOSONAR

    Mono<OrderQueryResponse> getSameDaySameRecipientFlorists(OrderQueryRequest orderQueryRequest);
}

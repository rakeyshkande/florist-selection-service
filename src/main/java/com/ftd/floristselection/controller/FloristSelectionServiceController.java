package com.ftd.floristselection.controller;


import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiResponse;
import com.ftd.floristselection.service.FloristSelectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@Api(tags = "Florist Selection API")
@RequestMapping(value = "/{siteId}/api/fss",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class FloristSelectionServiceController {

    private static final Logger LOG = LoggerFactory.getLogger(FloristSelectionServiceController.class);
    private FloristSelectionService floristSelectionService;

    @Autowired
    public FloristSelectionServiceController(FloristSelectionService floristSelectionService) {
        this.floristSelectionService = floristSelectionService;
    }

    @Timed
    @ExceptionMetered
    @PostMapping(value = "/selectFillingFlorist")
    @ApiOperation(value = "select filling member for the requested order")
    public Mono<FloristSelectionApiResponse> selectFillingFlorist(
            @PathVariable String siteId,
            @Valid @RequestBody FloristSelectionApiRequest request, BindingResult result) {

        LOG.info("Received request for selectFillingFlorist {}, {} ", siteId, request);

        return floristSelectionService.selectFillingFlorist(siteId, request);
    }
}

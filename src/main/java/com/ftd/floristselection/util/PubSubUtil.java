package com.ftd.floristselection.util;

import com.ftd.floristselection.api.entity.SourceCodeInfo;
import com.ftd.floristselection.pubsub.SourceCodeChangePubSubMessage;
import org.apache.commons.collections.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class PubSubUtil {
    private PubSubUtil() {

    }

    public static SourceCodeInfo buildSourceCode(SourceCodeChangePubSubMessage message) {
        SourceCodeInfo.SourceCodeInfoBuilder builder = SourceCodeInfo.builder();
        builder.sourceCode(message.getSourceCode())
                .oscarScenarioGroup(message.getOscarScenarioGroupName())
                .oscarSelectionEnabledFlag(PubSubUtil.convertFlag(message.getOscarScenarioGroupName()))
                .primaryBackupRWDFlag(PubSubUtil.convertFlag(message.getPrimaryBackupRWDFlag()))
                .timestamp(timeStamp(message.getTimestamp()));

        SourceCodeChangePubSubMessage.Preferences preferences = message.getPreferences();
        if (Objects.nonNull(preferences)) {
            builder.preferredFlorists(getFlorists(preferences.getPreferredFloristsForSourceCode()))
                    .secondaryFlorists(getFlorists(preferences.getSecondaryFloristsForSourceCode()))
                    .primaryFlorists(getFlorists(preferences.getPrimaryFloristForSourceCode()));
        }
        return builder.build();
    }

    private static LocalDateTime timeStamp(LocalDateTime localDateTime) {
        if (Objects.nonNull(localDateTime)) {
            return localDateTime;
        }
        return LocalDateTime.now();
    }

    private static Boolean convertFlag(String literal) {
        Boolean flag = Boolean.FALSE;
        if ("Y".equalsIgnoreCase(literal)) {
            flag = Boolean.TRUE;
        }
        return flag;
    }

    private static List<String> getFlorists(List<SourceCodeChangePubSubMessage.Member> florists) {
        if (CollectionUtils.isNotEmpty(florists)) {
            return florists.stream().map(SourceCodeChangePubSubMessage.Member::getFloristId).collect(Collectors.toList());
        }
        return null;
    }
}

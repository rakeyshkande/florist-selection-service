package com.ftd.floristselection.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class CustomJacksonStringDeserializer extends StdScalarDeserializer<String> {

    private static final long serialVersionUID = 1L;

    public CustomJacksonStringDeserializer() {
        super(String.class);
    }

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String value = new StringDeserializer().deserialize(p, ctxt);
        if (StringUtils.isBlank(value)) {
            return value;
        } else {
            return value.trim();
        }
    }

    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer)
            throws IOException {
        return deserialize(p, ctxt);
    }

}

package com.ftd.floristselection.util;

import com.ftd.floristselection.api.FloristData;
import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiResponse;
import com.ftd.floristselection.api.FloristSelectionFact;
import com.ftd.floristselection.api.entity.SourceCodeInfo;
import com.ftd.floristselection.config.AppConfigProperties;
import com.ftd.floristselection.constants.CommonConstants;
import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.IdentityInfoResponse;
import com.ftd.floristselection.ws.client.member.domain.MemberInfoResponse;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryRequest;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import com.ftd.oscar.athena.selector.AthenaRequest;
import com.ftd.oscar.athena.selector.AthenaRequestedFillingMember;
import com.ftd.oscar.athena.selector.ObjectFactory;
import com.ftd.oscar.athena.selector.SelectFillingMember;
import com.ftd.oscar.athena.selector.SelectFillingMemberResponse;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.common.util.concurrent.MoreExecutors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author avaka
 */
public final class FloristSelectionUtil {

    private static SecureRandom rand = new SecureRandom();

    private FloristSelectionUtil() {

    }

    private static final Logger LOG = LoggerFactory.getLogger(FloristSelectionUtil.class);

    public static String getProductId(FloristSelectionApiRequest.Product product) {
        if (Objects.nonNull(product)) {
            return product.getCode();
        }
        return null;
    }

    public static BigDecimal getProductPrice(FloristSelectionApiRequest.Product product) {
        if (Objects.nonNull(product)) {
            return product.getPrice();
        }
        return null;
    }

    public static String getZipCode(FloristSelectionApiRequest.Recipient recipient) {
        if (Objects.nonNull(recipient) && Objects.nonNull(recipient.getAddress())) {
            return recipient.getAddress().getPostalCode();
        }
        return null;
    }

    public static Mono<FloristData> extractFloristData(MemberInfoResponse memberInfoResponse) {
        FloristData.FloristDataBuilder builder = FloristData.builder();
        if (Objects.nonNull(memberInfoResponse)) {
            builder.id(memberInfoResponse.getId());
            IdentityInfoResponse.Identity identity = memberInfoResponse.getIdentity();
            if (Objects.nonNull(identity)) {
                builder.memberCode(identity.getMemberCode());
                builder.memberNumber(identity.getMemberId());
            }
            MemberInfoResponse.SubscriptionInfo subscriptionInfo = memberInfoResponse.getSubscriptionInfo();
            if (Objects.nonNull(subscriptionInfo) && CollectionUtils.isNotEmpty(subscriptionInfo.getSubscriptions())) {
                builder.hasMercury(isSubscribed(subscriptionInfo.getSubscriptions(),
                        subscriptionPredicate(CommonConstants.POS, null)));
                builder.isGoto(isSubscribed(subscriptionInfo.getSubscriptions(),
                        subscriptionPredicate(CommonConstants.FLORIST_FEATURE, CommonConstants.GOTO_FLORIST)));
            }
        }
        return Mono.just(builder.build());
    }

    private static Boolean isSubscribed(List<MemberInfoResponse.Subscription> subscriptions,
                                        Predicate<MemberInfoResponse.Subscription> subscriptionPredicate) {
        return subscriptions.stream().anyMatch(subscriptionPredicate);
    }

    private static Predicate<MemberInfoResponse.Subscription> subscriptionPredicate(String type, String name) {
        Predicate<MemberInfoResponse.Subscription> subscriptionPredicate = subscription -> false;
        if (Objects.nonNull(type) && Objects.isNull(name)) {
            subscriptionPredicate = subscription -> type.equalsIgnoreCase(subscription.getType());
        } else if (Objects.nonNull(name) && Objects.isNull(type)) {
            subscriptionPredicate = subscription -> name.equalsIgnoreCase(subscription.getName());
        } else if (Objects.nonNull(name) && Objects.nonNull(type)) {
            subscriptionPredicate = subscription -> name.equalsIgnoreCase(subscription.getName())
                    && type.equalsIgnoreCase(subscription.getType());
        }
        return subscriptionPredicate;
    }

    public static FloristSelectionFact extractFacts(String sourceCode, List<FloristData> floristDataList) {
        FloristSelectionFact floristSelectionFact = null;
        if (CollectionUtils.isNotEmpty(floristDataList)) {
            floristSelectionFact = FloristSelectionFact.builder()
                    .sourceCode(sourceCode)
                    .floristDataList(floristDataList)
                    .build();
        }
        return floristSelectionFact;
    }

    public static List<String> getGotoFlorists(List<FloristData> floristDataList) {
        return floristDataList.stream().map(floristData -> {
            if (Objects.nonNull(floristData.getIsGoto()) && floristData.getIsGoto()) {
                return floristData.getMemberCode();
            }
            return "";
        }).filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }

    public static List<String> getHasMercuryFlorists(List<FloristData> floristDataList) {
        return floristDataList.stream().map(floristData -> {
            if (Objects.nonNull(floristData.getHasMercury()) && floristData.getHasMercury()) {
                return floristData.getMemberCode();
            }
            return "";
        }).filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }

    public static List<String> getAvailableFlorists(List<AvailabilityInfoResponse> availabilityList) {
        if (CollectionUtils.isNotEmpty(availabilityList)) {
            List<AvailabilityInfoResponse> availableDates = availabilityList.stream()
                    .filter(item -> CollectionUtils.isNotEmpty(item.getAvailableFlorists()))
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(availableDates)) {
                availableDates.sort(Comparator.comparing(AvailabilityInfoResponse::getDeliveryDate));
                return availableDates.get(0).getAvailableFlorists();
            }
        }
        return null;
    }

    public static List<String> getFlorists(List<FloristData> floristDataList) {
        return floristDataList.stream().map(FloristData::getMemberCode).filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }

    public static Boolean includeDateForDelivery(AvailabilityInfoResponse response, List<String> excludedFlorists) {
        Boolean include = Boolean.FALSE;
        if (Objects.nonNull(response) && CollectionUtils.isNotEmpty(response.getAvailableFlorists())) {
            if (CollectionUtils.isNotEmpty(excludedFlorists)) {
                response.getAvailableFlorists().removeAll(excludedFlorists);
            }
            if (CollectionUtils.isNotEmpty(response.getAvailableFlorists())) {
                include = Boolean.TRUE;
            }
        }
        return include;
    }

    public static Boolean oscarEligibility(List<String> availableFlorists, List<String> filteredFlorists) {
        Boolean oscarEligibility = Boolean.TRUE;
        availableFlorists.retainAll(filteredFlorists);
        if (availableFlorists.size() == 1) {
            oscarEligibility = Boolean.FALSE;
        }
        return oscarEligibility;
    }

    public static SelectFillingMember getOscarRequest(AvailabilityInfoResponse response, String scenarioGroup,
                                                      FloristSelectionApiRequest request) throws DatatypeConfigurationException {
        ObjectFactory objectFactory = new ObjectFactory();
        SelectFillingMember selectFillingMember = new SelectFillingMember();

        AthenaRequest athenaRequest = objectFactory.createAthenaRequest();
        athenaRequest.setClientCode(CommonConstants.FS_SERVICE);
        athenaRequest.setClientReference(request.getMasterOrderNo());
        athenaRequest.setDeliveryDate(getXMLGregorianDeliveryDate(response));
        athenaRequest.setRecipientLatitude(request.getRecipient().getAddress().getGeoCode().getLatitude());
        athenaRequest.setRecipientLongitude(request.getRecipient().getAddress().getGeoCode().getLongitude());
        athenaRequest.setScenarioGroupCode(scenarioGroup);
        athenaRequest.setSendingMemberCode(request.getSendingMemberCode());

        List<String> availableFlorists = response.getAvailableFlorists();

        availableFlorists.forEach(florist -> {
            AthenaRequestedFillingMember athenaRequestedFillingMember =
                    objectFactory.createAthenaRequestedFillingMember();
            athenaRequestedFillingMember.setDistanceToRecipient(0.0f);
            athenaRequestedFillingMember.setMemberCode(florist);
            athenaRequest.getFillingMember().add(athenaRequestedFillingMember);
        });

        selectFillingMember.setRequest(athenaRequest);
        return selectFillingMember;
    }

    private static XMLGregorianCalendar getXMLGregorianDeliveryDate(AvailabilityInfoResponse response)
            throws DatatypeConfigurationException {
        XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        LocalDate deliveryDate = response.getDeliveryDate();
        xmlCal.setYear(deliveryDate.getYear());
        xmlCal.setMonth(deliveryDate.getMonthValue());
        xmlCal.setDay(deliveryDate.getDayOfMonth());
        return xmlCal;
    }

    public static Boolean nonNullOscarResponse(SelectFillingMemberResponse oscarResponse) {
        if (Objects.nonNull(oscarResponse)
                && Objects.nonNull(oscarResponse.getResponse())
                && StringUtils.isNotEmpty(oscarResponse.getResponse().getSelectedMember())) {
            return true;
        }
        return false;
    }

    public static FloristSelectionApiResponse getApiResponse(AvailabilityInfoResponse response) {
        FloristSelectionApiResponse.FloristSelectionApiResponseBuilder builder = FloristSelectionApiResponse.builder();
        if (Objects.nonNull(response)
                && CollectionUtils.isNotEmpty(response.getAvailableFlorists())) {
            builder.deliveryDate(response.getDeliveryDate());
            List<String> filteredFlorists = response.getAvailableFlorists();
            if (filteredFlorists.size() == 1) {
                builder.selectedMember(filteredFlorists.get(0));
            } else {
                int randomSelectedFlorist = rand.nextInt(filteredFlorists.size());
                builder.selectedMember(filteredFlorists.get(randomSelectedFlorist));
            }
        }
        return builder.build();
    }

    public static void handleDeliveryDate(FloristSelectionApiRequest apiRequest) {
        LocalDate today = LocalDate.now();
        if (apiRequest.getDeliveryDate().isBefore(today)
                && (apiRequest.getDeliveryDateEnd().isEqual(today) || apiRequest.getDeliveryDateEnd().isAfter(today))) {
            apiRequest.setDeliveryDate(today);
        }
    }

    public static List<String> getListByPriority(List<List<String>> priorityList) {
        for (List<String> aList :
                priorityList) {
            if (CollectionUtils.isNotEmpty(aList)) {
                return aList;
            }
        }
        return null;
    }

    public static void consumerListener(Subscriber subscriber) {
        subscriber.addListener(new Subscriber.Listener() {
            @Override
            public void failed(Subscriber.State from, Throwable failure) {
                LOG.error("error occurred while subscribing for subscription: {} , {}, {} ",
                        subscriber.getSubscriptionNameString(), from, failure.getMessage());
            }

            @Override
            public void starting() {
                LOG.info("{} subscriber is ready to consume requests", subscriber.getSubscriptionNameString());
            }
        }, MoreExecutors.directExecutor());
    }

    public static SourceCodeInfo getDefaultSourceCode(AppConfigProperties.Defaults defaults) {
        return SourceCodeInfo.builder()
                .sourceCode(defaults.getSourceCode())
                .oscarScenarioGroup(defaults.getScenarioGroup()).build();
    }

    public static String scenarioGroup(SourceCodeInfo sourceCodeInfo, AppConfigProperties appConfigProperties) {
        if (sourceCodeInfo != null && sourceCodeInfo.getOscarSelectionEnabledFlag()) {
            return sourceCodeInfo.getOscarScenarioGroup();
        }
        return appConfigProperties.getDefaults().getScenarioGroup();
    }

    public static OrderQueryRequest getOrderSearchRequest(LocalDate deliveryDate, String address1, String postalCode) {
        OrderQueryRequest.OrderQueryRequestBuilder builder = OrderQueryRequest.builder();
        builder.includedField(CommonConstants.FULFILLING_MEMBER);
        builder.sort(OrderQueryRequest.Sort.builder().fieldName(CommonConstants.ORDER_TIMESTAMP)
                .sortOrder(OrderQueryRequest.SortOrder.desc).build());
        builder.queryField(queryField(CommonConstants.DELIVERY_DATE_QUERY_FIELD, deliveryDate.toString()));
        builder.queryField(queryField(CommonConstants.ZIP_CODE_QUERY_FIELD, postalCode));
        builder.queryField(queryField(CommonConstants.ADDRESS_QUERY_FIELD, address1));
        builder.queryField(queryField(CommonConstants.FULFILLMENT_CHANNEL_QUERY_FIELD,
                CommonConstants.FLORIST_FULFILLMENT));
        builder.queryField(queryField(CommonConstants.STATUS_QUERY_FIELD, CommonConstants.WITH_FLORIST));
        return builder.build();
    }

    private static OrderQueryRequest.QueryField queryField(String field, String pattern) {
        OrderQueryRequest.QueryField.QueryFieldBuilder builder = OrderQueryRequest.QueryField.builder();
        String[] fields = {field};
        builder.fields(fields);
        builder.pattern(pattern);
        return builder.build();
    }

    public static List<OrderQueryResponse.DeliveryGroup> getDeliveryGroups(OrderQueryResponse response) {
        if (Objects.nonNull(response) && CollectionUtils.isNotEmpty(response.getOrderList())) {
            return response.getOrderList()
                    .stream()
                    .filter(order -> CollectionUtils.isNotEmpty(order.getDeliveryGroups()))
                    .flatMap(order -> order.getDeliveryGroups().stream())
                    .filter(deliveryGroup -> StringUtils.isNotEmpty(deliveryGroup.getFulfillingMember()))
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}

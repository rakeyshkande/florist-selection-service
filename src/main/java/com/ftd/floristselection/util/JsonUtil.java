package com.ftd.floristselection.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.File;
import java.io.IOException;


public final class JsonUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        objectMapper.setSerializationInclusion(Include.NON_EMPTY);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        SimpleModule module = new SimpleModule();
        module.addDeserializer(String.class, new CustomJacksonStringDeserializer());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(module);
    }

    private JsonUtil() {
        super();
    }

    public static String asJsonString(final Object obj) throws JsonProcessingException {
        if (obj == null) {
            return null;
        }
        return objectMapper.writeValueAsString(obj);
    }

    public static <T> T getJsonObj(File filename, Class<T> valueType) throws IOException {
        return objectMapper.readValue(filename, valueType);
    }

    public static <T> T getJsonObj(String jsonString, Class<T> valueType) throws IOException {
        return objectMapper.readValue(jsonString, valueType);
    }

    public static <T> T getJsonObj(String jsonString, TypeReference<T> valueTypeRef) throws IOException {
        return objectMapper.readValue(jsonString, valueTypeRef);
    }


}

package com.ftd.floristselection.service;

import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface FloristSelectionService {
    Mono<FloristSelectionApiResponse> selectFillingFlorist(String siteId, FloristSelectionApiRequest apiRequest);
}

package com.ftd.floristselection.service.impl;

import com.ftd.commons.misc.exception.BusinessException;
import com.ftd.floristselection.api.FloristData;
import com.ftd.floristselection.api.FloristSelectionApiRequest;
import com.ftd.floristselection.api.FloristSelectionApiResponse;
import com.ftd.floristselection.api.entity.SourceCodeInfo;
import com.ftd.floristselection.config.AppConfig;
import com.ftd.floristselection.constants.CommonConstants;
import com.ftd.floristselection.repository.SourceCodeRepository;
import com.ftd.floristselection.service.FloristSelectionService;
import com.ftd.floristselection.util.FloristSelectionUtil;
import com.ftd.floristselection.ws.client.floristavailability.AvailabilityService;
import com.ftd.floristselection.ws.client.floristavailability.domain.AvailabilityInfoResponse;
import com.ftd.floristselection.ws.client.member.MemberService;
import com.ftd.floristselection.ws.client.ordersearch.OrderSearchService;
import com.ftd.floristselection.ws.client.ordersearch.domain.OrderQueryResponse;
import com.ftd.floristselection.ws.client.oscarselector.OscarSelectorService;
import com.ftd.oscar.athena.selector.SelectFillingMemberResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author avaka
 */
@Service
public class FloristSelectionServiceImpl implements FloristSelectionService {

    private AppConfig appConfig;
    private MemberService memberService;
    private OrderSearchService orderSearchService;
    private AvailabilityService availabilityService;
    private SourceCodeRepository sourceCodeRepository;
    private OscarSelectorService oscarSelectorService;
    private static final Logger LOG = LoggerFactory.getLogger(FloristSelectionServiceImpl.class);

    @Autowired
    public FloristSelectionServiceImpl(AppConfig appConfig,
                                       MemberService memberService,
                                       OrderSearchService orderSearchService,
                                       AvailabilityService availabilityService,
                                       SourceCodeRepository sourceCodeRepository,
                                       OscarSelectorService oscarSelectorService) {
        this.appConfig = appConfig;
        this.memberService = memberService;
        this.orderSearchService = orderSearchService;
        this.availabilityService = availabilityService;
        this.sourceCodeRepository = sourceCodeRepository;
        this.oscarSelectorService = oscarSelectorService;
    }

    @Override
    public Mono<FloristSelectionApiResponse> selectFillingFlorist(String siteId, FloristSelectionApiRequest apiRequest) {

        FloristSelectionUtil.handleDeliveryDate(apiRequest);
        return getAvailabilityInfo(siteId,
                FloristSelectionUtil.getProductId(apiRequest.getProduct()),
                FloristSelectionUtil.getProductPrice(apiRequest.getProduct()),
                FloristSelectionUtil.getZipCode(apiRequest.getRecipient()),
                apiRequest.getDeliveryDate(),
                apiRequest.getDeliveryDateEnd(),
                apiRequest.getExcludedFlorists())
                .concatMap(response ->
                        deliveryDateEligibility(siteId, response, apiRequest))
                .filter(response -> CollectionUtils.isNotEmpty(response.getAvailableFlorists()))
                .next()
                .flatMap(response ->
                        Mono.just(FloristSelectionUtil.getApiResponse(response)))
                .switchIfEmpty(Mono.error(new BusinessException(HttpStatus.NOT_FOUND.name(),
                        CommonConstants.NO_FLORIST_FOUND, HttpStatus.NOT_FOUND)))
                .log();
    }

    private Mono<AvailabilityInfoResponse> deliveryDateEligibility(String siteId,
                                                                   AvailabilityInfoResponse response,
                                                                   FloristSelectionApiRequest apiRequest) {

        List<String> availableFlorists = response.getAvailableFlorists();
        if (availableFlorists.size() == 1) {
            return Mono.just(response);
        }

        return getSameDaySameRecipientFlorists(response.getDeliveryDate(),
                apiRequest.getRecipient().getAddress().getAddressLine1(),
                apiRequest.getRecipient().getAddress().getPostalCode())
                .concatMap(florist -> Mono.just(florist))
                .filter(florist -> availableFlorists.contains(florist))
                .next()
                .flatMap(florist -> {
                    availableFlorists.retainAll(Arrays.asList(florist));
                    return Mono.just(response);
                })
                .switchIfEmpty(deliveryDateEligibility2(siteId, response, apiRequest));
    }

    private Mono<AvailabilityInfoResponse> deliveryDateEligibility2(String siteId,
                                                                    AvailabilityInfoResponse response,
                                                                    FloristSelectionApiRequest apiRequest) {
        List<String> availableFlorists = response.getAvailableFlorists();
        return getSourceCodeInfo(apiRequest.getSourceCode())
                .flatMap(sourceCodeInfo -> {
                    List<String> primaryAvailableFlorists = ListUtils.intersection(availableFlorists,
                            ListUtils.emptyIfNull(sourceCodeInfo.getPrimaryFlorists()));
                    List<String> backupAvailableFlorists = ListUtils.intersection(availableFlorists,
                            ListUtils.emptyIfNull(sourceCodeInfo.getSecondaryFlorists()));
                    List<String> preferredAvailableFlorists = ListUtils.intersection(availableFlorists,
                            ListUtils.emptyIfNull(sourceCodeInfo.getPreferredFlorists()));
                    List<String> priorityList = FloristSelectionUtil.getListByPriority(
                            Arrays.asList(primaryAvailableFlorists, backupAvailableFlorists, preferredAvailableFlorists)
                    );

                    Mono<List<String>> filteredFlorists;
                    if (CollectionUtils.isNotEmpty(priorityList)) {
                        filteredFlorists = Mono.just(priorityList);
                    } else {
                        filteredFlorists = getFloristsData(siteId, availableFlorists)
                                .flatMap(floristDataList -> {
                                    List<String> gotoFlorists =
                                            FloristSelectionUtil.getGotoFlorists(floristDataList);
                                    List<String> hasMercuryFlorists =
                                            FloristSelectionUtil.getHasMercuryFlorists(floristDataList);

                                    return Mono.just(FloristSelectionUtil.getListByPriority(
                                            Arrays.asList(gotoFlorists, hasMercuryFlorists, availableFlorists)
                                    ));
                                });
                    }

                    return filteredFlorists
                            .flatMap(florists -> {
                                Boolean aBoolean = FloristSelectionUtil.oscarEligibility(availableFlorists, florists);
                                if (aBoolean) {
                                    return filterFlorists(response,
                                            FloristSelectionUtil.scenarioGroup(sourceCodeInfo,
                                                    appConfig.getAppConfigProperties()),
                                            apiRequest)
                                            .flatMap(oscarResponse -> {
                                                if (FloristSelectionUtil.nonNullOscarResponse(oscarResponse)) {
                                                    ArrayList<String> selectedFlorist = new ArrayList<>();
                                                    selectedFlorist.add(oscarResponse.getResponse().getSelectedMember());
                                                    response.getAvailableFlorists()
                                                            .retainAll(selectedFlorist);
                                                }
                                                return Mono.just(response);
                                            });
                                }
                                return Mono.just(response);
                            });
                });
    }

    private Flux<AvailabilityInfoResponse> getAvailabilityInfo(String siteId, String productId,
                                                               BigDecimal productPrice, String zipCode,
                                                               LocalDate deliveryDate, LocalDate deliveryDateEnd,
                                                               List<String> excludedFlorists) {
        return availabilityService.getAvailableFlorists(siteId, productId, productPrice, zipCode, deliveryDate, deliveryDateEnd)
                .filter(availabilityInfoResponse -> FloristSelectionUtil.
                        includeDateForDelivery(availabilityInfoResponse, excludedFlorists));
    }

    private Mono<SourceCodeInfo> getSourceCodeInfo(String sourceCode) {
        SourceCodeInfo defaultSourceCode = FloristSelectionUtil
                .getDefaultSourceCode(appConfig.getAppConfigProperties().getDefaults());
        return sourceCodeRepository.get(sourceCode)
                .onErrorReturn(throwable -> {
                    LOG.error("#getSourceCodeInfo: {}", throwable);
                    return true;
                }, defaultSourceCode)
                .defaultIfEmpty(defaultSourceCode)
                .log();
    }

    private Mono<List<FloristData>> getFloristsData(String siteId, List<String> memberCodes) {
        return memberService.getIdentityInfo(siteId, memberCodes)
                .parallel()
                .runOn(Schedulers.parallel())
                .flatMap(identity -> memberService.getMemberInfo(siteId, identity.getId()))
                .sequential()
                .flatMap(FloristSelectionUtil::extractFloristData)
                .log()
                .collectList()
                .onErrorReturn(throwable -> {
                    LOG.error("#getFloristsData: {}", throwable);
                    return true;
                }, new ArrayList<>());
    }

    private Mono<SelectFillingMemberResponse> filterFlorists(AvailabilityInfoResponse response, String scenarioGroup,
                                                             FloristSelectionApiRequest request) {
        return Mono.fromCallable(() -> oscarSelectorService
                .selectFillingMember(FloristSelectionUtil.getOscarRequest(response, scenarioGroup, request)))
                .subscribeOn(Schedulers.elastic())
                .onErrorReturn(throwable -> {
                    LOG.error("#callOscarService: {}", throwable);
                    return true;
                }, new SelectFillingMemberResponse());
    }

    private Flux<String> getSameDaySameRecipientFlorists(LocalDate deliveryDate, String address1, String postalCode) {
        return orderSearchService.getSameDaySameRecipientFlorists(FloristSelectionUtil.getOrderSearchRequest(
                deliveryDate, address1, postalCode))
                .flatMapIterable(FloristSelectionUtil::getDeliveryGroups)
                .map(OrderQueryResponse.DeliveryGroup::getFulfillingMember);
    }
}

FROM gcr.io/gcp-ftd-prod-devops/openjdk:v8-slim
MAINTAINER ftd
ADD target/*.jar /
ADD src/main/resources /resources
ADD entrypoint.sh /entrypoint.sh
ENV PATH /:$PATH
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

